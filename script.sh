FUNC_TYPE=$1


ARG_2=$2
ARG_3=$3

IDENTITY_NAME=$ARG_2
EMAIL_PK=$ARG_3
ARG_4=$4
ARG_5=$5
ARG_6=$6
ARG_7=$7
ARG_8=$8
create_account() {
    snet identity create --private-key $EMAIL_PK $IDENTITY_NAME key
    snet identity $IDENTITY_NAME
    snet network ropsten
}

account_switch() {
    snet identity $IDENTITY_NAME
}

network_switch() {
    snet network ropsten
}

get_channel(){
    snet channel print-initialized-filter-org nunet-org default_group  --filter-sender
}

get_balance(){
    snet identity $IDENTITY_NAME
    snet account balance
}

deposit() {
    snet account deposit 0.001 -y
}

create_channel() {
    snet identity $IDENTITY_NAME
    snet account deposit 0.000001 -y
    snet channel open-init nunet-org default_group 1 +20days -y
}

extend_date() {
snet account deposit 0.0001 -y
snet channel extend-add-for-org nunet-org default_group --expiration +10days -y
}

amount_add() {
snet account deposit 0.000001 -y
snet channel extend-add-for-org nunet-org default_group --amount 0.000001 -y
}



case $FUNC_TYPE in
    create_account) create_account ;;
    get_balance)    get_balance ;;
    deposit)        deposit ;;
    account_switch) account_switch ;;
    network_switch) network_switch ;;

    get_channel)    get_channel ;;
    create_channel) create_channel ;;
    extend_date)    extend_date ;;
    amount_add)     amount_add ;;


esac
